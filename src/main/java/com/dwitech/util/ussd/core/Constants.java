package com.dwitech.util.ussd.core;

/**
 *
 */
public class Constants {
	public final static String ENCODING = "UTF-8";
	public final static String CONTENT_TYPE = "application/json";
	public final static String MYSQL_JDBC_DRIVER = "com.mysql.jdbc.Driver";
	public final static String DB_USERNAME = "root";
	public final static String DB_PASSWORD = "AqVCGNrR";
	private final static String DB_HOSTNAME = "localhost";
	private final static String DB_PORT = "3306";
	private final static String DB_NAME = "ussd_processor";
	public final static String DB_URL = "jdbc:mysql://" + DB_HOSTNAME + ":" + DB_PORT + "/" + DB_NAME;
}
